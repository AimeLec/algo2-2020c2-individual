#include <iostream>
#include <list>
using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();
    void incrementar_dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    bool operator<(Fecha o);

  private:
    //Completar miembros internos
    int mes_;
    int dia_;
};

bool Fecha::operator<(Fecha o) {
    bool res = false;
    if(this->mes() < o.mes()) {
        res = true;
    } else if(this->mes() > o.mes()) {
    } else {
        if(this->dia() < o.dia())
            res = true;
    }
    return res;
}
Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {}

int Fecha::mes() {
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << f.dia() << "/" << f.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = (this->dia() == o.dia()) && (this->mes() == o.mes());
    return igual_dia;
}
#endif

void Fecha::incrementar_dia() {
    dia_++;
    if(dia_ > dias_en_mes(mes_)){
        dia_ = 1;
        mes_++;
    }
}
// Ejercicio 11, 12

// Clase Horario
class Horario{
    public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario h);
        bool operator<(Horario h);
    private:
        uint hora_;
        uint min_;
};

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    bool mismo_horario = (this->hora() == h.hora()) && (this->min() == h.min());
    return mismo_horario;
}

bool Horario::operator<(Horario h) {
    bool res = false;
    if(this->hora() < h.hora()) {
        res = true;
    } else if(this->hora() > h.hora()) {
    } else {
        if(this->min() < h.min())
            res = true;
    }
    return res;
}
// Ejercicio 13

// Clase Recordatorio
class Recordatorio {
public:
    Recordatorio(Fecha f, Horario h, string s);
    Fecha fech();
    Horario hor();
    string mensaje();
    bool operator<(Recordatorio rec);
private:
    Fecha f_;
    Horario h_;
    string msg_;
};

bool Recordatorio::operator<(Recordatorio rec) {
    bool res = false;
    if(this->fech() < rec.fech())
        res = true;
    else if (this->fech() == rec.fech()){
        if(this->hor() < rec.hor())
            res = true;
    }
    return res;
}

Recordatorio::Recordatorio(Fecha f, Horario h, string s) : f_(f), h_(h), msg_(s) {}

Fecha Recordatorio::fech() {
    return f_;
}
Horario Recordatorio::hor() {
    return h_;
}

string Recordatorio::mensaje() {
    return msg_;
}

ostream& operator<<(ostream& os, Recordatorio rec) {
    os << rec.mensaje() << " @ " << rec.fech() << " " << rec.hor();
    return os;
}

// Ejercicio 14

// Clase Agenda
class Agenda {
public:
    Agenda(Fecha Fecha_actual);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    Fecha hoy_;
    list<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha Fecha_actual) : hoy_(Fecha_actual){}

Fecha Agenda::hoy() {
    return hoy_;
}

void Agenda::incrementar_dia() {
    hoy_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
    recordatorios_.sort();
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> recsDeHoy;
    for(Recordatorio s : recordatorios_) {
        if(s.fech() == hoy()) {
            recsDeHoy.push_back(s);
        }
    }
    return recsDeHoy;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << endl
       << "=====" << endl;
    for(Recordatorio rec : a.recordatorios_de_hoy()) {
        os << rec << endl;
    }
    return os;
}