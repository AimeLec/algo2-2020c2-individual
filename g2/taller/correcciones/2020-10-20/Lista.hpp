#include "Lista.h"

Lista::Lista() : _head(nullptr), _tail(nullptr), _long(0) {
    // Completar
}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    // Completar
    Nodo *temp = _head;
    while (_head != nullptr) {
        temp = temp->siguiente;
        delete _head;
        _head = temp;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo *temp = _head;
    while (_head != nullptr) {
        temp = temp->siguiente;
        delete _head;
        _head = temp;
    }
    _long = 0;
    _tail = NULL;
    for(int i = 0; i<aCopiar.longitud(); i++) {
        agregarAtras(aCopiar.iesimo(i));
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    // Completar
    if(longitud() == 0){
        _head = new Nodo(elem);
        _tail = _head;
    } else {
        Nodo* nodo_nuevo = new Nodo(elem);
        _head->anterior = nodo_nuevo;
        nodo_nuevo->siguiente = _head;
        _head = nodo_nuevo;
    }
    _long++;

}

void Lista::agregarAtras(const int& elem) {
    // Completar
    if(longitud() == 0){
        _head = new Nodo(elem);
        _tail = _head;
    } else {
        Nodo* nodo_nuevo = new Nodo(elem);
        _tail->siguiente = nodo_nuevo;
        nodo_nuevo->anterior = _tail;
        _tail = nodo_nuevo;
    }
    _long++;
}

void Lista::eliminar(Nat i) {
    if(_long==1){
        delete _head;
        _long--;
        _head = nullptr;
    }else if(i==0){
        Nodo* temp = _head->siguiente;
        delete _head;
        _long--;
        _head = temp;
        _head->anterior = nullptr;
    } else if(i == _long-1){
        Nodo* temp = _tail->anterior;
        delete _tail;
        _long--;
        _tail = temp;
        _tail->siguiente = nullptr;
    }else{
        Nodo* actual = _head;
        for (int j = 0; j < i; ++j) {
            actual = actual->siguiente;
        }
        Nodo* tempNext = actual->siguiente;
        Nodo* tempPrev = actual->anterior;
        delete actual;
        tempNext->anterior = tempPrev;
        tempPrev->siguiente = tempNext;
        _long--;
    };
}

int Lista::longitud() const {
    // Completar
    return _long;
}

const int& Lista::iesimo(Nat i) const {
    // Completar
    Nodo* nodo_actual = _head;
    for(int j = 0; j<i; j++) {
        nodo_actual = nodo_actual->siguiente;
    }
    return nodo_actual->valor;
}

int& Lista::iesimo(Nat i) {
    // Completar (hint: es igual a la anterior...)
    Nodo* nodo_actual = _head;
    for(int j = 0; j<i; j++) {
        nodo_actual = nodo_actual->siguiente;
    }
    return nodo_actual->valor;
}

void Lista::mostrar(ostream& o) {
    // Completar
}
