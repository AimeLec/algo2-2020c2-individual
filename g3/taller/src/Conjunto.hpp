
template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL) {
    // Completar
}
template<class T>
void Conjunto<T>::destruir(Nodo *n) {
    if(n!=NULL) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template <class T>
Conjunto<T>::~Conjunto() { 
    destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return pertenecer(_raiz, clave);
}

template<class T>
bool Conjunto<T>::pertenecer(Nodo *n, const T &clave) const{
    bool aparicion = false;
    if(n != NULL) {
        if(n->valor == clave) {
            aparicion = true;
        } else if(n->valor > clave) {
            aparicion = pertenecer(n->izq, clave);
        } else if(n->valor < clave) {
            aparicion = pertenecer(n->der, clave);
        }
    }
    return aparicion;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(_raiz == NULL) {
        _raiz = new Nodo(clave);
    } else {
        insercion(_raiz, NULL, 'd', clave);
    }
}

template<class T>
void Conjunto<T>::insercion(Nodo *n, Nodo* padre, char dir, const T &clave) {
    if(n == NULL) {
        n = new Nodo(clave);
        n->padre = padre;
        if(dir == 'd') {
            padre->der = n;
        } else {
            padre->izq = n;
        }
    } else {
        if(n->valor < clave) {
            insercion(n->der, n, 'd', clave);
        } else if (n->valor > clave) {
            insercion(n->izq, n, 'i', clave);
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(_raiz->valor == clave && _raiz->izq == NULL && _raiz->der == NULL) {
        delete _raiz;
        _raiz = NULL;
    } else {
        borrar(_raiz, NULL, 'd', clave);
    }
}

template<class T>
void Conjunto<T>::borrar(Nodo *n, Nodo *padre, char dir, const T &clave) {
    if(n!=NULL){
        if(n->valor == clave) {
            if(n->izq == NULL && n->der == NULL) {
                if(dir == 'd') {
                    padre->der = NULL;
                } else {
                    padre->izq = NULL;
                }
                delete n;
            } else {
                if(n->der != NULL) {
                    n->valor = min(n->der);
                    borrar(n->der, n, 'd', n->valor);
                } else {
                    n->valor = max(n->izq);
                    borrar(n->izq, n, 'i', n->valor);
                }
            }
        } else if (n->valor < clave) {
            borrar(n->der, n, 'd', clave);
        } else {
            borrar(n->izq, n, 'i', clave);
        }
    }

}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    return sucesor(_raiz, clave);
}

template<class T>
const T& Conjunto<T>::sucesor(Nodo *n, const T& clave) {
    if(n->valor < clave) {
        sucesor(n->der, clave);
    } else if (n->valor > clave) {
        sucesor(n->izq, clave);
    } else {
        hallarSucesor(n, clave);
    }
}

template<class T>
const T& Conjunto<T>::hallarSucesor(Nodo *n, const T &clave) {
    if (n->der != NULL) {
        return min(n->der);
    } else {
        Nodo* tmp = n->padre;
        while(tmp != NULL && n == tmp->der){
            n = tmp;
            tmp = tmp->padre;
        }
        return tmp->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    min(_raiz);
}

template<class T>
const T & Conjunto<T>::min(Nodo *n) const{
    if(n->izq == NULL) {
        return n->valor;
    } else {
        min(n->izq);
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    max(_raiz);
}

template<class T>
const T & Conjunto<T>::max(Nodo *n) const{
    if(n->der == NULL) {
        return n->valor;
    } else {
        max(n->der);
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return cant(_raiz);
}

template<class T>
unsigned int Conjunto<T>::cant(Nodo *n) const{
    unsigned int card = 0;
    if(n != NULL) {
        card = cant(n->izq) + cant(n->der) + 1;
    }
    return card;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

