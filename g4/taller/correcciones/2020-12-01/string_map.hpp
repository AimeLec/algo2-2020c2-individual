template <typename T>
string_map<T>::string_map(): raiz(new Nodo()), _size(0){
    // COMPLETAR
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    _size = d._size;
    destruir(raiz);
    delete raiz;
    raiz = new Nodo();
    construir(raiz, d.raiz);
}

template<typename T>
void string_map<T>::construir(Nodo* a, Nodo* b) {
    if(b->definicion != nullptr) {
        a->definicion = new T;
        *(a->definicion) = *(b->definicion);
    }
    for(int i = 0; i<256; i++) {
        if(b->siguientes[i] != nullptr) {
            a->siguientes[i] = new Nodo();
            construir(a->siguientes[i], b->siguientes[i]);
        }
    }
}

template <typename T>
string_map<T>::~string_map(){
    destruir(raiz);
    delete raiz;
}

template<typename T>
void string_map<T>::destruir(Nodo* a) {
    if(a->definicion != nullptr) {
        delete a->definicion;
    }
    for(int i = 0; i<256; i++) {
        if(a->siguientes[i] != nullptr) {
            destruir(a->siguientes[i]);
            delete a->siguientes[i];
        }
    }
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}
template<typename T>
void string_map<T>::insert(const pair<string, T>& a) {
    if(raiz == nullptr) {
        raiz = new Nodo();
    }
    _size++;

    Nodo* nodo_actual = raiz;
    for(int i=0; i<a.first.size(); i++) {
        if(nodo_actual->siguientes[int(a.first[i])] == nullptr)
            nodo_actual->siguientes[int(a.first[i])] = new Nodo();
        nodo_actual = nodo_actual->siguientes[int(a.first[i])];
    }

    if(nodo_actual->definicion != nullptr) {
        delete nodo_actual->definicion;
    }

    nodo_actual->definicion = new T;
    *(nodo_actual->definicion) = a.second;
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    int res = 1;
    if(raiz == nullptr){
        res = 0;
    }
    Nodo* nodo_actual = raiz;
    for(int i=0; res != 0 && i<clave.size(); i++) {
        if(nodo_actual->siguientes[int(clave[i])] == nullptr) {
            res = 0;
        } else {
            nodo_actual = nodo_actual->siguientes[int(clave[i])];
        }
    }

    if(res != 0) {
        if(nodo_actual->definicion == nullptr) {
            res = 0;
        }
    }

    return res;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* nodo_actual = raiz;

    for(int i=0; i<clave.size(); i++) {
        nodo_actual = nodo_actual->siguientes[int(clave[i])];
    }

    return *(nodo_actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* nodo_actual = raiz;

    for(int i=0; i<clave.size(); i++) {
        nodo_actual = nodo_actual->siguientes[int(clave[i])];
    }

    return *(nodo_actual->definicion);
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    _size--;
    Nodo* nodo_actual = raiz;
    Nodo* ultimo_nodo = raiz;
    int ultimo_indice = 0;
    for(int i = 0; i<clave.size(); i++) {
        int hijos = 0;
        for(int j = 0; j<256; j++) {
            if(nodo_actual->siguientes[j] != nullptr) {
                hijos++;
            }
        }

        if(hijos >1 || nodo_actual->definicion != nullptr) {
            ultimo_indice = i;
            ultimo_nodo = nodo_actual;
        }

        nodo_actual = nodo_actual->siguientes[int(clave[i])];
    }

    int hijos = 0;
    for(int j = 0; j<256; j++) {
        if(nodo_actual->siguientes[j] != nullptr) {
            hijos++;
        }
    }
    if(hijos >0) {
        delete nodo_actual->definicion;
        nodo_actual->definicion = nullptr;
    } else {
        destruir(ultimo_nodo->siguientes[int(clave[ultimo_indice])]);
        delete ultimo_nodo->siguientes[int(clave[ultimo_indice])];
        ultimo_nodo->siguientes[int(clave[ultimo_indice])] = nullptr;
    }
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return (_size == 0);
}